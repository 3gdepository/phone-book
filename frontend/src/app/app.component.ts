import {Component, OnInit} from '@angular/core';
import {Contact} from './model/contact';
import {ContactService} from './service/contact.service';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public contacts: Contact[];

  constructor(private contactService: ContactService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.updateTable();
  }

  delete($event: Contact): void {
    this.contacts.splice(this.contacts.indexOf($event));
    this.updateTable();
  }

  edit($event: Contact): void {
    const contactForm = this.modalService.open(ContactFormComponent);
    contactForm.componentInstance.init($event);
    contactForm.result.then(() => this.updateTable(), () => {});
  }

  updateTable(): void {
    this.contactService.getAllContacts()
      .subscribe(result => {
        this.contacts = result;
      });
  }
}
