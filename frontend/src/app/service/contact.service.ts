import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Contact} from '../model/contact';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) {
  }

  getAllContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>('http://localhost:8080/api/contact/all');
  }

  addContacts(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>('http://localhost:8080/api/contact', contact);
  }

  updateContact(contact: Contact): Observable<void> {
    return this.http.put<void>(`http://localhost:8080/api/contact/${contact.id}`, contact);
  }

  deleteContact(contactId: number): Observable<void> {
    return this.http.delete<void>(`http://localhost:8080/api/contact/${contactId}`);
  }

}
