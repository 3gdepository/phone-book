import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Contact} from '../model/contact';
import {ContactService} from '../service/contact.service';

@Component({
  selector: 'app-contact-row',
  templateUrl: './contact-row.component.html',
  styleUrls: ['./contact-row.component.css']
})
export class ContactRowComponent implements OnInit {

  @Input() public contact: Contact;
  @Output() public onDelete: EventEmitter<Contact> = new EventEmitter<Contact>();
  @Output() public onEdit: EventEmitter<Contact> = new EventEmitter<Contact>();

  constructor(private contactService: ContactService) {
  }

  ngOnInit(): void {
  }


  delete(): void {
    this.contactService.deleteContact(this.contact.id)
      .subscribe(() => {
        this.onDelete.emit(this.contact);
      });
  }

  edit(): void {
    this.onEdit.emit(this.contact);
  }
}
