import {Component, Directive, OnInit, ViewContainerRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ContactService} from '../service/contact.service';
import {Contact} from '../model/contact';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[contactForm]',
})
export class ContactFormDirective {
  constructor(public viewContainerRef: ViewContainerRef) {
  }
}

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  form = new FormGroup({
    id: new FormControl(''),
    firstName: new FormControl('', Validators.required),
    secondName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required)
  });

  constructor(private contactService: ContactService,
              public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }

  save(): void {
    if (this.form.value.id === '') {
      this.contactService.addContacts(this.form.value).subscribe(() => {
        this.activeModal.close();
      });
    } else {
      this.contactService.updateContact(this.form.value)
        .subscribe(() => {
          this.activeModal.close();
        });
    }
  }

    init(contact: Contact): void {
      this.form.patchValue(contact);
    }
}
