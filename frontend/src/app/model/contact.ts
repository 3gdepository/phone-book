export interface Contact {
  id: number;
  firstName: string;
  secondName: string;
  phoneNumber: string;
}
