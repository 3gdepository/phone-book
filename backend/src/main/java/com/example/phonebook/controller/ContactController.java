package com.example.phonebook.controller;

import com.example.phonebook.entity.Contact;
import com.example.phonebook.model.ContactModel;
import com.example.phonebook.model.CreateContactModel;
import com.example.phonebook.model.UpdateContactModel;
import com.example.phonebook.service.ContactService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/contact")
public class ContactController {

    final
    private ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContactModel> getById(@PathVariable("id") Long id) {
        Optional<Contact> contactOptional = contactService.getById(id);
        if(contactOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        ContactModel contactModel = ContactModel.fromEntity(contactOptional.get());
        return ResponseEntity.ok(contactModel);
    }

    @GetMapping("/all")
    public ResponseEntity<List<ContactModel>> getAll() {
        List<Contact> list = contactService.getAllContacts();
        List<ContactModel> contactList = list.stream().map(ContactModel::fromEntity).collect(Collectors.toList());

        return ResponseEntity.ok(contactList);
    }

    @PostMapping
    public ResponseEntity<ContactModel> create(@RequestBody CreateContactModel createContactModel){
        Contact contact = contactService.create(createContactModel);

        return ResponseEntity.ok(ContactModel.fromEntity(contact));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ContactModel> update(@PathVariable("id") Long id, @RequestBody UpdateContactModel updateContactModel){
        Contact contact = contactService.update(updateContactModel, id);

        return ResponseEntity.ok(ContactModel.fromEntity(contact));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        contactService.delete(id);

        return ResponseEntity.ok().build();
    }

}
