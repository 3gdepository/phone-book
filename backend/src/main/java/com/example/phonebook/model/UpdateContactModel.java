package com.example.phonebook.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateContactModel {

    private String firstName;

    private String secondName;

    private String phoneNumber;


}
