package com.example.phonebook.model;

import com.example.phonebook.entity.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateContactModel {

    private Long id;

    private String firstName;

    private String secondName;

    private String phoneNumber;

    public Contact modelToEntity() {
        final Contact contact = new Contact();
        contact.setId(getId());
        contact.setFirstName(getFirstName());
        contact.setSecondName(getSecondName());
        contact.setPhoneNumber(getPhoneNumber());

        return contact;
    }


}
