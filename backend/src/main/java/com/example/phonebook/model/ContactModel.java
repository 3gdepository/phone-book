package com.example.phonebook.model;

import com.example.phonebook.entity.Contact;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactModel {

    private Long id;

    private String firstName;

    private String secondName;

    private String phoneNumber;

    public static ContactModel fromEntity(final Contact contact) {
        ContactModel contactModel = new ContactModel();
        contactModel.setId(contact.getId());
        contactModel.setFirstName(contact.getFirstName());
        contactModel.setSecondName(contact.getSecondName());
        contactModel.setPhoneNumber(contact.getPhoneNumber());

        return contactModel;
    }


}
