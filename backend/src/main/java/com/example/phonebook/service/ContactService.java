package com.example.phonebook.service;

import com.example.phonebook.entity.Contact;
import com.example.phonebook.model.CreateContactModel;
import com.example.phonebook.model.UpdateContactModel;
import com.example.phonebook.repository.ContactRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactService {

    final
    ContactRepository contactRepository;

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public Optional<Contact> getById(final Long id) {
        return contactRepository.findById(id);
    }

    public void delete(Long id) {
        contactRepository.deleteById(id);
    }

    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    public Contact update(UpdateContactModel updateContactModel, Long id) throws IllegalArgumentException {
        Optional<Contact> contactOptional = contactRepository.findById(id);
        if (contactOptional.isEmpty()) {
            throw new IllegalArgumentException("no such user");
        } else {
            Contact storedContact = contactOptional.get();
            storedContact.setFirstName(updateContactModel.getFirstName());
            storedContact.setSecondName(updateContactModel.getSecondName());
            storedContact.setPhoneNumber(updateContactModel.getPhoneNumber());
            return contactRepository.save(storedContact);
        }
    }

    public Contact create(final CreateContactModel createContactModel) {
        return contactRepository.save(createContactModel.modelToEntity());
    }

}
