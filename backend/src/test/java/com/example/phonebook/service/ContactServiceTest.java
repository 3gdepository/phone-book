package com.example.phonebook.service;

import com.example.phonebook.entity.Contact;
import com.example.phonebook.model.CreateContactModel;
import com.example.phonebook.repository.ContactRepository;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class ContactServiceTest {
    private final ContactRepository repository = mock(ContactRepository.class);
    private final ContactService service = new ContactService(repository);

    @Test
    void getById() {
        Contact contact = new Contact(1L, "Test", "Test", "+xxxxx");
        when(repository.findById(contact.getId())).thenReturn(Optional.of(contact));
        Optional<Contact> contactOptional = service.getById(contact.getId());
        assertThat(contactOptional.isPresent()).isTrue();
        assertThat(contactOptional.get()).isEqualTo(contact);
    }

    @Test
    void getById_wheNotFound() {
        when(repository.findById(anyLong())).thenReturn(Optional.empty());
        Optional<Contact> contactOptional = service.getById(1L);
        assertThat(contactOptional.isPresent()).isFalse();
    }

    @Test
    void delete() {
        service.delete(1L);
        verify(repository).deleteById(1L);
    }

    @Test
    void getAllContacts() {
        Contact contact1 = new Contact(1L, "Test", "Test", "+xxxxx");
        Contact contact2 = new Contact(2L, "Test", "Test", "+xxxxx");
        when(repository.findAll()).thenReturn(List.of(contact1, contact2));
        List<Contact> contacts = service.getAllContacts();
        assertThat(contacts).hasSize(2);
        assertThat(contacts).containsExactly(contact1, contact2);
    }

    @Test
    void create() {
        when(repository.save(any())).then(input -> input.getArgument(0, Contact.class));
        CreateContactModel contactModel = new CreateContactModel(1L, "Test", "Test", "+xxxxx");
        Contact contact = service.create(contactModel);
        assertThat(contact).isEqualTo(contactModel.modelToEntity());
    }
}
