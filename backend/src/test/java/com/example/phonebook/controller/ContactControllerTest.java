package com.example.phonebook.controller;

import com.example.phonebook.entity.Contact;
import com.example.phonebook.model.ContactModel;
import com.example.phonebook.model.CreateContactModel;
import com.example.phonebook.model.UpdateContactModel;
import com.example.phonebook.service.ContactService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class ContactControllerTest {
    public static final Contact CONTACT_1 = new Contact(1L, "test", "test", "+xxxx");
    public static final Contact CONTACT_2 = new Contact(2L, "test", "test", "+xxxx");

    private final ContactService contactService = mock(ContactService.class);
    private final ContactController contactController = new ContactController(contactService);

    @BeforeEach
    void setUp() {
        when(contactService.getById(-1L)).thenReturn(Optional.empty());
        when(contactService.getById(1L)).thenReturn(Optional.of(CONTACT_1));
        when(contactService.getById(2L)).thenReturn(Optional.of(CONTACT_2));
        when(contactService.getAllContacts()).thenReturn(List.of(CONTACT_1, CONTACT_2));
        when(contactService.create(any(CreateContactModel.class)))
                .then(input -> ((CreateContactModel) input.getArgument(0)).modelToEntity());
        when(contactService.update(any(UpdateContactModel.class), anyLong()))
                .then(input -> new Contact(input.getArgument(1, Long.class),
                    input.getArgument(0, UpdateContactModel.class).getFirstName(),
                    input.getArgument(0, UpdateContactModel.class).getSecondName(),
                    input.getArgument(0, UpdateContactModel.class).getPhoneNumber()));
    }

    @Test
    void getById() {
        ResponseEntity<ContactModel> response = contactController.getById(1L);
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(ContactModel.fromEntity(CONTACT_1));
    }

    @Test
    void getById_whenNonExists() {
        ResponseEntity<ContactModel> response = contactController.getById(-1L);
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void getAll() {
        ResponseEntity<List<ContactModel>> response = contactController.getAll();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasSize(2);
        assertThat(response.getBody()).containsExactly(ContactModel.fromEntity(CONTACT_1), ContactModel.fromEntity(CONTACT_2));
    }

    @Test
    void create() {
        ArgumentCaptor<CreateContactModel> captor = ArgumentCaptor.forClass(CreateContactModel.class);
        CreateContactModel createContactModel = new CreateContactModel(3L, "test", "test", "+xxx");
        ResponseEntity<ContactModel> response = contactController.create(createContactModel);
        verify(contactService).create(captor.capture());
        assertThat(captor.getValue()).isEqualTo(createContactModel);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(new ContactModel(3L, "test", "test", "+xxx"));
    }

    @Test
    void update() {
        ResponseEntity<ContactModel> response = contactController.update(1L, new UpdateContactModel("newTest", "newTest", "+yyy"));
        verify(contactService).update(any(UpdateContactModel.class), eq(1L));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(new ContactModel(1L, "newTest", "newTest", "+yyy"));
    }

    @Test
    void delete() {
        ResponseEntity<Void> response = contactController.delete(1L);
        verify(contactService).delete(1L);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
